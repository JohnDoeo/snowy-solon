/*
 * Copyright [2022] [https://www.xiaonuo.vip]
 *
 * Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Snowy源码头部的版权声明。
 * 3.本项目代码可免费商业使用，商业使用请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://www.xiaonuo.vip
 * 5.不可二次分发开源参与同类竞品，如有想法可联系团队xiaonuobase@qq.com商议合作。
 * 6.若您的项目无法满足以上几点，需要更多功能代码，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.core.config;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.http.ContentType;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.solon.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.solon.plugins.inner.PaginationInnerInterceptor;
import org.noear.solon.Solon;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.core.handle.Action;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.SessionState;
import org.noear.solon.web.staticfiles.StaticMappings;
import org.noear.solon.web.staticfiles.repository.ClassPathStaticRepository;
import vip.xiaonuo.common.annotation.CommonNoRepeat;
import vip.xiaonuo.common.listener.CommonDataChangeEventCenter;
import vip.xiaonuo.common.listener.CommonDataChangeListener;
import vip.xiaonuo.common.pojo.CommonResult;

import java.lang.reflect.Method;
import java.util.List;

/**
 * Snowy配置
 *
 * @author xuyuxiang
 * @date 2021/10/9 14:24
 **/
@Configuration
public class GlobalConfigure {

    /**
     * 静态资源映射
     *
     * @author xuyuxiang
     * @date 2022/7/25 15:16
     **/
    @Bean
    public void addResourceHandlers() {
        StaticMappings.add("/doc.html",  new ClassPathStaticRepository("/META-INF/resources/"));
        StaticMappings.add("/webjars/",  new ClassPathStaticRepository("/META-INF/resources/webjars/"));
    }

    /**
     * 添加节流防抖拦截器
     *
     * @author xuyuxiang
     * @date 2022/6/20 15:18
     **/
    @Bean
    public void addInterceptors() {
        Solon.app().before(ctx -> {
            Action action = ctx.action();
            if(action != null){
                Method method = action.method().getMethod();
                CommonNoRepeat annotation = method.getAnnotation(CommonNoRepeat.class);
                if (ObjectUtil.isNotEmpty(annotation)) {
                    if (this.isRepeatSubmit(ctx, annotation)) {
                        ctx.charset(CharsetUtil.UTF_8);
                        ctx.contentType(ContentType.JSON.toString());
                        ctx.outputAsJson(JSONUtil.toJsonStr(CommonResult.error("请求过于频繁，请稍后再试")));
                        ctx.setHandled(true);
                    }
                }
            }
        });
    }

    protected boolean isRepeatSubmit(Context ctx, CommonNoRepeat annotation) {
        JSONObject jsonObject = JSONUtil.createObj();
        jsonObject.set("repeatParam", JSONUtil.toJsonStr(ctx.paramMap()));
        jsonObject.set("repeatTime", DateUtil.current());
        String url = ctx.url();
        SessionState session = ctx.sessionState();
        Object sessionObj = session.sessionGet("repeatData");
        if (ObjectUtil.isNotEmpty(sessionObj)) {
            JSONObject sessionJsonObject = JSONUtil.parseObj(sessionObj);
            if(sessionJsonObject.containsKey(url)) {
                JSONObject existRepeatJsonObject = sessionJsonObject.getJSONObject(url);
                if (jsonObject.getStr("repeatParam").equals(existRepeatJsonObject.getStr("repeatParam")) &&
                        jsonObject.getLong("repeatTime") - existRepeatJsonObject.getLong("repeatTime") < annotation.interval()) {
                    return true;
                }
            }
        }
        session.sessionSet("repeatData", JSONUtil.createObj().set(url, jsonObject));
        return false;
    }


    /**
     * 分页插件
     *
     * @author xuyuxiang
     * @date 2022/3/11 10:59
     **/
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
        mybatisPlusInterceptor.addInnerInterceptor(new PaginationInnerInterceptor());
        return mybatisPlusInterceptor;
    }

    /**
     * 注册数据变化事件中心 事件发布器
     *
     * @author xuyuxiang
     * @date 2023/3/3 14:27
     **/
    @Bean
    public void registerListenerList(List<CommonDataChangeListener> dataChangeListenerList) {
        CommonDataChangeEventCenter.registerListenerList(dataChangeListenerList);
    }
}
