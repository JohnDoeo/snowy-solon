package vip.xiaonuo.core.config;

import org.apache.ibatis.mapping.DatabaseIdProvider;
import org.noear.solon.annotation.Component;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * 数据库id选择器，用于Mapper.xml中
 * MyBatis可以根据不同的数据库厂商执行不同的语句
 *
 * @author xuyuxiang
 * @date 2022/1/8 2:16
 */
@Component
public class CustomDbIdProvider implements DatabaseIdProvider {

    @Override
    public String getDatabaseId(DataSource dataSource) throws SQLException {
        String url = dataSource.getConnection().getMetaData().getURL();
        if (url.contains("oracle")) {
            return "oracle";
        } else if (url.contains("postgresql")) {
            return "pgsql";
        } else if (url.contains("mysql")) {
            return "mysql";
        } else if (url.contains("dm")) {
            return "dm";
        } else if (url.contains("kingbase")) {
            return "kingbase";
        }  else {
            return "mysql";
        }
    }
}

